﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KontaktListan_TE
{
    public partial class EditContactForm : Form
    {
        private Contact _contact;

        public EditContactForm(Contact contact)
        {
            InitializeComponent();
            _contact = contact;
            tbxEmail.Text = contact.Email;
            tbxFirstName.Text = contact.FirstName;
            tbxLastName.Text = contact.LastName;
            tbxPhonenumber.Text = contact.PhoneNumber;
            dateTimePicker1.Value = contact.DateOfBirth;
        }

        private void btnOK_Click(object sender, EventArgs e)
        {
            _contact.DateOfBirth = dateTimePicker1.Value;
            _contact.Email = tbxEmail.Text;
            _contact.FirstName = tbxFirstName.Text;
            _contact.LastName = tbxLastName.Text;
            _contact.PhoneNumber = tbxPhonenumber.Text;

            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
