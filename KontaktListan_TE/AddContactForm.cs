﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KontaktListan_TE
{
    public partial class AddContactForm : Form
    {
        public AddContactForm()
        {
            InitializeComponent();
        }

        public Contact Contact { get; set; }

        private void btnOK_Click(object sender, EventArgs e)
        {
            Contact = new Contact
            {
                FirstName = tbxFirstName.Text,
                LastName = tbxLastName.Text,
                Email = tbxEmail.Text,
                PhoneNumber = tbxPhonenumber.Text,
                DateOfBirth = dateTimePicker1.Value
            };

            DialogResult = DialogResult.OK;
            Close();

        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }
    }
}
