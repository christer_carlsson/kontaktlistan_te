﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KontaktListan_TE
{
    public partial class ViewContact : Form
    {
        private Contact _contact;

        public ViewContact(Contact contact)
        {
            InitializeComponent();
            _contact = contact;
            lblEmail.Text = contact.Email;
            lblPhoneNumber.Text = contact.PhoneNumber;
            lblDoB.Text = contact.DateOfBirth.ToLongDateString();
            this.Text = contact.FullName;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.OK;
            Close();
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            DialogResult restult = new EditContactForm(_contact).ShowDialog();

            Close(); 
        }
    }
}
