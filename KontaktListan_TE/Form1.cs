﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KontaktListan_TE
{
    public partial class Form1 : Form
    {

        List<Contact> _contacts = new List<Contact>();
        const string filename = "contacts.csv";


        public Form1()
        {
            InitializeComponent();
            
        }

        private void btn_DeleteContact_Click(object sender, EventArgs e)
        {
            Contact contact;
            if(TryGetContactFromList(out contact)) {
                listBox_Contacts.Items.Remove(contact);
                _contacts.Remove(contact);
            }
        }

        private void btn_AddContact_Click(object sender, EventArgs e)
        {
            var addForm = new AddContactForm();

            if(addForm.ShowDialog() == DialogResult.OK)
            {
                listBox_Contacts.Items.Add(addForm.Contact);
                _contacts.Add(addForm.Contact);
            }

        }

        private void SaveContacts()
        {

            using (StreamWriter sw = new StreamWriter(filename))
            {
                foreach (var contact in _contacts)
                {
                    string result = string.Empty;
                    result += contact.FirstName + ";";
                    result += contact.LastName + ";";
                    result += contact.Email + ";";
                    result += contact.PhoneNumber + ";";
                    result += contact.DateOfBirth.ToShortDateString();

                    sw.WriteLine(result);
                    
                }
                sw.Flush();
            }
        }

        private void LoadContacts()
        {
            if (File.Exists(filename))
            {
                using (StreamReader sr = new StreamReader(filename))
                {
                    while (!sr.EndOfStream)
                    {
                        string curContactStr = sr.ReadLine();
                        string[] curContactArr = curContactStr.Split(';');
                        Contact c = new Contact
                        {
                            FirstName = curContactArr[0],
                            LastName = curContactArr[1],
                            Email = curContactArr[2],
                            PhoneNumber = curContactArr[3],
                            DateOfBirth = DateTime.Parse(curContactArr[4])
                        };
                        _contacts.Add(c);
                        listBox_Contacts.Items.Add(c);
                    }
                }
            }
        }
            private void listBox_Contacts_DoubleClick(object sender, MouseEventArgs e)
        {
            Contact contact = listBox_Contacts.SelectedItem as Contact;

            if (contact == null)
                return;

            new ViewContact(contact).ShowDialog();

            ReloadUI();
        }

        private void ReloadUI()
        {
            listBox_Contacts.Items.Clear();
            foreach (Contact contact in _contacts)
            {
                listBox_Contacts.Items.Add(contact);
            }

        }

        private bool TryGetContactFromList(out Contact contact)
        {
            contact = listBox_Contacts.SelectedItem as Contact;

            if (contact == null)
                return false;
            return true;
        }

        private void tbx_Search_TextChanged(object sender, EventArgs e)
        {
            listBox_Contacts.Items.Clear();

            foreach (Contact contact in _contacts)
            {
                if (contact.FirstName.ToLower().Contains(tbx_Search.Text.ToLower()) ||
                    contact.LastName.ToLower().Contains(tbx_Search.Text.ToLower()) ||
                    contact.Email.ToLower().Contains(tbx_Search.Text.ToLower())
                    )
                {
                    listBox_Contacts.Items.Add(contact);
                }
            }
        }

        private void Form1_OnLoad(object sender, EventArgs e)
        {
            LoadContacts();
        }

        private void Form1_OnClosing(object sender, FormClosingEventArgs e)
        {
            SaveContacts();
        }
    }
}
